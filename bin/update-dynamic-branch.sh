#!/bin/bash
#
#
# Create a Gitlab Pipeline Schedule:
#   Interval Pattern:            0 23 * * *
#   Cron timezone:               [UTC+1] Stockholm
#   Select target branch or tag: use-head-from-all-submodules
#
#
#



set -euo pipefail
set -x

TARGET_BRANCH="${1:-use-head-from-all-submodules}"


WORKDIR=$(mktemp -d)



function extract_prod_version() {
    curl --silent 'https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/raw/master/bin/run_prod.sh?ref_type=heads' \
        | grep "^PIPELINECOMMIT=[[:alnum:]]*$" \
        | sed 's|^PIPELINECOMMIT=\([[:alnum:]]*\)$|\1|'
}



function checkout_prod_version() {
    local ver="$1"

    git fetch
    git checkout "$TARGET_BRANCH"
    git merge -m "Automerge $ver" "$ver"
    git submodule update --init --recursive
}



function update_submodule_versions() {
    for sub in $(git submodule status | awk '{print $2}'); do
        echo "INFO: update $sub" >&2
        pushd "$sub"
        git fetch
        default_branch=$(git remote show origin | sed -n '/HEAD branch/s/.*: //p')
        git checkout "$default_branch"
        git pull origin "$default_branch"
        popd
        echo "INFO: done updating $sub" >&2
    done
}



function commit_and_push() {
    if git add . \
            && git commit -m "Update all submodules to their default branch HEADs" ; then
        LANG=C git push -o ci.skip origin "$TARGET_BRANCH"
    fi
}



PRODVER=$(extract_prod_version)

checkout_prod_version "$PRODVER"
update_submodule_versions
commit_and_push
