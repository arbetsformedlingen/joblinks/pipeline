#!/usr/bin/env bash
set -eEu -o pipefail

# nexus push address
nexus_host=docker-images.jobtechdev.se


function add_arg() {
    local -n hash="$1"
    local name="$2"
    local arg="$3"

    local list=${hash["$name"]:-}
    list+=" $arg"
    hash["$name"]=${list}
}

function is_skipped() {
    local -n slist=$1
    local cmd=$2

    if [ -n "${slist[@]}" ] && [[ " ${slist[@]} " =~ " $cmd " ]]; then
        return 0
    fi
    return 1
}

function log() {
    echo "$(date) pipeline:  $*" >&2
}

function die() {
    log "$@"
    exit 1;
}

function logdest() {
    local debug="$1"; shift
    local logfile="$1"; shift

    if [ "$debug" = 0 ]; then
        $@ 2>>"${logfile}"
    else
        $@
    fi
}

function join_by {
    local d="$1"
    local -n f="$2"
    local out=""
    local first=1

    for index in "${!f[@]}" ; do
        local elem=${f[$index]}
        if [ "$first" = 1 ]; then
            out+="$elem"; first=0
        else
            out+="$d $elem"
        fi
    done

    echo "$out"
}

# this is a miniature pipeline processor
function trunc() {
    if [ -n "$limit" ]; then
        jq -c '.[0:'"$limit"']'
    else
        cat -
    fi
}
export -f trunc



# this is a miniature pipeline processor
function trunc_jsonl() {
    if [ -n "$limit" ]; then
        head -n "$limit"
    else
        cat -
    fi
}
export -f trunc


# Standard step runner, creating uniform output and log files
function proc() {
    local debug="${1}"; shift
    local ldir="${1}"; shift
    local program="${1}"; shift
    local logfile="${ldir}/${program}".log
    local timefile="${ldir}/${program}".time
    local statusfile="${ldir}/${program}".status
    local outfile="${ldir}/${program}".out
    local exe="$1"

    # bash functions cannot be timed, so only time real commands
    local timecmd=
    echo "---- check exe $exe" >&2
    if which "$exe" >/dev/null 2>/dev/null || [[ -x "$exe" ]]; then
        timecmd="/usr/bin/time -p -o ${timefile}"
    fi

    log "starting processor $program: $@"
    logdest "${debug}" "${logfile}" "${timecmd}" $@ | tee ${outfile} \
        || { local _s=$?; echo $_s > "${statusfile}"; echo "***** $program failed ($_s)!" >&2; \
              test -f "${logfile}" && tail "${logfile}" >&2 ; \
             return $_s; }
    local status=$?

    echo $status > "${statusfile}"
    return $status
}
export -f proc


function determine_source_program() {
    local -n orgpipe="${1}"
    local startelem="${2}"
    local ldir="${3}"

    local lastelem=""
    for index in "${!orgpipe[@]}" ; do
        local all="$(echo ${orgpipe[$index]})"
        local cmd="${all%% *}"

        if [ "$cmd" = "$startelem" ]; then
            break;
        fi

        if [ -s "${ldir}/${cmd}".out ]; then
            lastelem="$cmd"
        fi
    done

    echo "${lastelem}"
}

# remove skipped commands from pipeline
function filter_pipeline_steps() {
    local -n pipel=$1              # is mutated in this procedure
    local -n skipped_datasource=$2 # is mutated in this procedure
    local    from_step=$3
    local    to_step=$4
    local -n steplist=$5           # is mutated in this procedure

    skipped_datasource=0 # first command is the pipeline source, needs special treatment
    for index in "${!pipel[@]}" ; do

        # extract the command label into 'cmd'
        local all="$(echo ${pipel[$index]})"
        local cmd="${all%% *}"

        if [ -n "${from_step}" ] && [ "$from_step" = "$cmd" ]; then
            from_step=""
        fi

        # delete pipeline step if any of the delete conditions are true
        if is_skipped steplist "$cmd" \
               || ( [ -n "${from_step}" ] && [ "$from_step" != "$cmd" ] ) \
               || ( [ -n "${to_step}" ] && [ "${to_step}" = "__delete" ] ); then
            skip_steps+=" $(echo "${pipel[$index]}" | awk '{print $1}' | tr '-' '_')"
            unset -v 'pipel[$index]'
            if [ "$index" = 0 ]; then
                skipped_datasource=1 # mutate
            fi
        fi

        if [ -n "${to_step}" ] && [ "$to_step" = "$cmd" ]; then
            to_step="__delete"
        fi
    done
    pipel=("${pipel[@]}") # mutate
    skip_steps=("${skip_steps[@]}") # mutate
}


function add_pipeline_args() {
    local -n pipel=$1
    local -n args=$2

    for index in "${!pipel[@]}" ; do
        # extract the command label into 'cmd'
        all=$(echo ${pipel[$index]})
        cmd=${all%% *}

        arg="${args[$cmd]:-}"

        if [ -n "$arg" ]; then
            pipel[$index]+="$arg"
        fi
    done
    pipel=("${pipel[@]}")
}



function gitrepo_dir() {
    local dir="$1"

    # run in sub process to not mess up CWD
    (
        cd "$dir" || return 1
        local githash="$(git rev-parse --short HEAD)"
        local url="$(git config --get remote.origin.url)"
        if [[ $url =~ ^.*gitlab.*$ ]]; then
            url="${url/.git/}"
            url="$(echo $url | sed 's|git@gitlab.com:|https://gitlab.com/|' | sed 's|://.*:.*@|://|' )"
            url="$url/-/tree/$githash"
        fi
        echo $url
    )
}



function githash_dir() {
    local dir="$1"

    # run in sub process to not mess up CWD
    (
        cd "$dir" || return 1
        local githash="$(git rev-parse --short HEAD)"
        echo "$githash"
    )
}

function gitver_dir() {
    local dir="$1"

    # run in sub process to not mess up CWD
    (
        cd "$dir" || return 1
        local githash="$(git rev-parse --short HEAD)"
        local label="$(git config --get remote.origin.url)"
        label=$(echo "$label" | sed -E 's|^.*[\:\/]([^\:\/\.]*).*?$|\1|')
        echo "$label:$githash"
    )
}



function make_main_report() {
    local -n pipelineref=$1
    local    ldir="$2"
    local    confname="$3"

    echo "$pipeline_name/$confname:"
    if [ -s "${ldir}"/pipeline."$pipeline_name".time ]; then
        echo -e "\tPipeline time (total, for all steps in $pipeline_name):"
        sed 's|^|\t\t|' < "${ldir}"/pipeline."$pipeline_name".time
        echo
    fi
    if [ -s "${ldir}"/pipeline."$pipeline_name".cmdline ]; then
        echo -e "\tPipeline invocation:"
        sed 's|^|\t\t|' < "${ldir}"/pipeline."$pipeline_name".cmdline
        echo
    fi
    echo -ne "\tPipeline exit status:\n\t\t"
    if [ "${status}" = 0 ]; then
        echo "PASS"
    else
        echo "FAIL"
    fi
    echo
    echo -e "\tPipeline steps:"
    for i in ${!pipelineref[@]}; do
        cmdline=(${pipelineref[$i]})

        # get version
        ver=""
        for x in $(seq 1 $(( ${#cmdline[@]} - 1 )) ); do
            if [[ ${cmdline[@]:$x:1} =~ ^.*processors.*run.sh$ ]]; then
                ver=$(${cmdline[@]:$x:1} --version)
                break
            fi
        done

        cmdline[1]=${cmdline[1]/$proj_root\//} # tidy up for printing
        echo -e "\t\tname: ${cmdline[@]:0:1}"
        echo -e "\t\tcmd:  ${cmdline[@]:1}"
        if [ -n "$ver" ]; then
            echo -e "\t\tver:  ${ver}"
        fi
        if [ -s "${ldir}"/"${cmdline[@]:0:1}".time ]; then
            echo -e "\t\ttime: "$(cat "${ldir}"/"${cmdline[@]:0:1}".time)
        fi
        if [ -s "${ldir}"/"${cmdline[@]:0:1}".status ]; then
            echo -e "\t\texit: "$(cat "${ldir}"/"${cmdline[@]:0:1}".status)
        fi
        echo
    done
    echo
    echo
    echo
    echo
}



function make_exec_summary() {
    local -n pipelineref=$1
    local    ldir="$2"
    local    confname="$3"
    local    gitrepo_dir="$4"
    local    gitver_dir="$5"
    local    today="$6"

    local    gitrepo_blob="$(echo $gitrepo_dir | sed 's|/tree/|/blob/|')"

    for i in ${!pipelineref[@]}; do
        local cmdline=(${pipelineref[$i]})
        echo -n "|[$pipeline_name/$confname/${cmdline[@]:0:1}]($gitrepo_blob/pipelines/$pipeline_name/$confname)|"

        # get version
        local ver=""
        local url=""
        for x in $(seq 1 $(( ${#cmdline[@]} - 1 )) ); do
            if [[ ${cmdline[@]:$x:1} =~ ^.*processors.*run.sh$ ]]; then
                ver="$(${cmdline[@]:$x:1} --version)"
                url="$(${cmdline[@]:$x:1} --repo)"
                break
            fi
        done

        if [ -s "${ldir}"/"${cmdline[@]:0:1}".status ]; then
            echo -n $(cat "${ldir}"/"${cmdline[@]:0:1}".status)"|"
        else
            echo -n "inline|"
        fi

        if [ -s "${ldir}"/"${cmdline[@]:0:1}".time ]; then
            echo -n $(cat "${ldir}"/"${cmdline[@]:0:1}".time | sed 's|^real ||i' | sed 's|user [0-9].*$||i' | sed 's|sys [0-9].*$||i')"|"
        else
            echo -n "n/a (bash function)|"
        fi

        echo -n "[log](https://pipeline.arbetsformedlingen.se/$namespace/$today/logs/${cmdline[@]:0:1}.log.gz) "
        echo -n "[out](https://pipeline.arbetsformedlingen.se/$namespace/$today/logs/${cmdline[@]:0:1}.out.gz)|"

        if [ -n "$ver" ] && [ -n "$url" ]; then
            echo "[${ver}]($url)|"
        else
            echo "[$gitver_dir]($gitrepo_dir)|"
        fi
    done
}



##################################
#### Build and deploy stuff below

function bash_require() {
    local req_maj=$1
    local req_min=$2

    local BV=(${BASH_VERSION//./ })
    if [[ ${BV[0]} -lt ${req_maj} ]] || ( [[ ${BV[0]} = ${req_maj} ]] && [[ ${BV[1]} -lt ${req_min} ]]); then
        return 1
    fi
    return 0
}

function verify_tools() {
    for tool in bash git sed curl docker jq; do
        command -v "${tool}" >/dev/null || { echo "*** please install ${tool}" >&2; exit 1; }
    done

    bash_require 4 3 || { echo "**** upgrade bash to >= 4.3" >&2; exit 1; }
}

function build_processor() {
    local dir="${1}"

    pushd "${dir}" || exit 1

    # the current directory name will do as a unique processor tag
    local label=joblinks/$(basename $PWD)

    local githash="$(git rev-parse --short HEAD)"

    if ! docker image inspect "${nexus_host}"/"${label}":"${githash}" >/dev/null 2>/dev/null; then
        local buildlog="$(mktemp)"

        docker build --no-cache -t "${nexus_host}"/"${label}" . | tee "${buildlog}"

        # podman pattern - put ID on the last line
        local id=$(tail -n 1 "${buildlog}")

        if docker --version|grep -q "^Docker"; then
            # docker pattern - put ID after "Successfully built "
            if grep -q -- '^Successfully built ' "${buildlog}"; then
                id=$(grep -- '^Successfully built ' "${buildlog}" | tail -n 1 | sed 's|^Successfully built ||')
            else
                die "Could not detect ID of built image"
            fi
        fi

        # add a tag 'label:githash', so we can later determine if this
        # git commit's image is already built
        local tag=$(docker image inspect "${id}" | jq -r '.[] | .RepoTags[]' | head -n 1) || exit 1

        docker tag "${tag}" "${nexus_host}"/"${label}":"${githash}" || exit 1

        rm -f "${buildlog}"
    else
        log "image tagged ${nexus_host}/${label}:${githash} found - reusing"
    fi
    popd
}

### REMOVE STUFF HERE - move build to devops too
function clean() {
    local proj_root="$1"

    if [ -d "${proj_root}" ]; then
        cd /tmp
        # "${proj_root}"/run.sh --clean    # unnecessary now when we have checks if rebuild is needed
        rm -rf "${proj_root}"
    fi
}
export -f clean

function build() {
    local proj_root="$1"

    if [ -d "${proj_root}" ]; then
        cd "${proj_root}" || exit 1
        local processor_dir=""
        for processor_dir in $(ls -1 "${proj_root}"/processors/*/Dockerfile); do
            build_processor $(dirname "${processor_dir}") || exit 1
        done
    else
        die "**** cannot find proj_root '${proj_root}'"
    fi
}

function run() {
    local proj_root="${1}"; shift

    build "${proj_root}" || exit 1
    cd "${proj_root}"
    bash bin/pipeline $@
}
