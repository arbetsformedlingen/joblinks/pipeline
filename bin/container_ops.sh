#!/usr/bin/env bash
set -eEu -o pipefail

# Detect the directory name of this script
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Load functions
. "${self_dir}/function_lib.sh"


if [ ! -f Dockerfile ]; then
    echo "**** error: no Dockerfile in $PWD">&2; exit 1
fi


#############################
#### PARSE COMMANDLINE

# pick up directories we want to share in container
dockerargs=
while [ "$#" -gt 0 ]; do
    case "$1" in
        --)             shift; break;;
        *)              dockerargs="$dockerargs $1";;
    esac
    shift
done
appargs="$@"

if [ "$#" -eq 0 ]; then
    appargs="$(echo $dockerargs | sed 's|^[ ]*||')"
    dockerargs=""
fi

if [ "$appargs" = "--build" ]; then
    build_processor "$PWD" ; exit $?
fi


if [ "$appargs" = "--version" ]; then
    echo $(gitver_dir "$PWD")
    exit
fi

if [ "$appargs" = "--repo" ]; then
    echo $(gitrepo_dir "$PWD")
    exit
fi



label=joblinks/$(basename $PWD)
githash="$(git rev-parse --short HEAD)"

if ! docker image inspect "${nexus_host}"/"${label}":"${githash}" >/dev/null 2>/dev/null; then
    echo "**** Build docker image ${nexus_host}/${label}:${githash} first">&2;
    exit 1
fi


if [ "$appargs" = "--push" ]; then
    PUSHARG=''
    if docker --version | grep -q podman; then
	PUSHARG="docker://${nexus_host}/$label:$githash"
    fi

    secrets="${self_dir}"/../secrets/test/docker-images.jobtechdev.se/config.json
    if [ ! -f "${secrets}" ]; then echo "**** cannot find secrets $secrets" >&2; exit 1; fi

    user=$(jq -r .user < "${secrets}")
    pass=$(jq -r .pass < "${secrets}")
    echo "$pass" | docker login -u "$user" --password-stdin "${nexus_host}" || exit 1

    exec docker push "${nexus_host}"/"$label:$githash" $PUSHARG
fi


if [ "$appargs" = "--cmd" ]; then
    echo docker run $dockerargs --rm -i "${nexus_host}"/"${label}":"${githash}"
    exit
fi


# Run the application
echo "exec container: docker run $dockerargs --rm -i ${nexus_host}/${label}:${githash} $appargs" >&2
exec docker run $dockerargs --rm -i "${nexus_host}"/"${label}":"${githash}" $appargs
