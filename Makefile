.ONESHELL:


## Determine which git branch is currently in use
BRANCH  = $(shell LANG=C git rev-parse --symbolic-full-name --abbrev-ref HEAD)


promote-proj:
	@for R in $$(grep submodule .gitmodules | sed -e 's|^.*processors/||' -e 's|".*$$||'); do
		if [ ! -d ../$$R ]; then
			echo "missing checked out repo for $$R" >&2; exit 1
		fi
	done
	DIR=$$(pwd)
	for R in $$(grep submodule .gitmodules | sed -e 's|^.*processors/||' -e 's|".*$$||'); do
		cd ../$$R
		make -f $$DIR/Makefile promote || exit 1
		cd - >/dev/null
	done
	# git checkout master
	# for S in $(git config --file=.gitmodules -l | grep branch= | sed 's|=.*$||'); do git config --file=.gitmodules $S master; done
	# git submodule sync
	# git submodule update --init --recursive --remote

promote: gitcheck
	@git pull origin master || exit 1
	if [ "$$(basename $$PWD)" = "scraping" ]; then
		git push origin develop-jobtech:master
	else
		git push origin develop:master
	fi


#### Helper targets - not to be run from command line directly #####


gitcheck:
	@if [ ! -z "$$(git status --porcelain)" ]; then
		echo "**** error: repo is not clean" >&2
		exit 1
	fi
	CMPBRANCH="develop"
	if [ "$$(basename $$PWD)" = "scraping" ]; then
		CMPBRANCH="develop-jobtech"
	fi
	if [ "$(BRANCH)" != "$$CMPBRANCH" ]; then
		echo "**** error: not on $$CMPBRANCH ($(BRANCH))" >&2
		exit 1
	fi
	git pull


gendocs: docs/devops.md docs/devops.1 docs/devops.txt docs/devops.html

docs/%.md: bin/%
	mkdir -p docs ; pod2markdown $< > $@

docs/%.1: bin/%
	mkdir -p docs ; pod2man $< > $@

docs/%.txt: bin/%
	mkdir -p docs ; pod2text $< > $@

docs/%.html: bin/%
	mkdir -p docs ; pod2html $< > $@
