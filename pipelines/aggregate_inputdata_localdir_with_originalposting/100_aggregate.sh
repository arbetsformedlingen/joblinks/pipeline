# Inputdata files in JSONL format, each line a Job Posting ad.
#
#
. "${proj_root}/conf/jobtechlinks.sh"

[[ -n "$inputdata_localdir" ]] || { echo "**** missing setting 'inputdata_localdir'" >&2; exit 1; }

json_validate_output=1
clitoolscmd="$($clitools --cmd)"


function quoted_jq() {
    $clitoolscmd jq -c '.'
}


pipeline=(
    # label                      # command line
    "aggregate                   cat $inputdata_localdir/*"
    "trunc                       trunc_jsonl"
    "prepare_ads                 quoted_jq"
)
