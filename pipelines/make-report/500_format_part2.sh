. "${proj_root}/conf/jobtechlinks.sh"

loglink_report="$(echo $(ls -1tr ${ldir}/pipeline.*.report.loglinks))"

pipeline=(
    # label                      # command line
    "report2                     cat <(echo -e '### Report [2/3] - execution summary (alpha, may fail)\n[batch runner log](https://pipeline.arbetsformedlingen.se/$namespace/$today/logs/out.log.gz)\n\n|Step|Exit|Time|Log|Version|\n| :--- | :--- | :--- | :--- | :--- |') $loglink_report <(echo -e '-----------------\nLog hyperlinks only work for prod reports.\nUse minio credentials for bucket pipeline.')"
)
