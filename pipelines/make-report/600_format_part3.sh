. "${proj_root}/conf/jobtechlinks.sh"

af_report="${ldir}/af_report.out"
main_report="${ldir}/main_report.out"


function format_report3() {
    echo -e '### Report [3/3] - statistics'
    echo '```'
    cat $main_report $af_report
    echo '```'
}


pipeline=(
    # label                      # command line
    "report3                     format_report3"
)
