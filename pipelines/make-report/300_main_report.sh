. "${proj_root}/conf/jobtechlinks.sh"

pipeinput="${ldir}/filter_for_import.out"

# the pipeline's commit is included in the report
export gitcommit="$(git log -1 --pretty='%h %d  %cd  %s' | tr -d "'")"

pipeline=(
    # label                      # command line
    "main_report                 $mkreport '${gitcommit}'"
)
