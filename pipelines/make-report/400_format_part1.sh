. "${proj_root}/conf/jobtechlinks.sh"

proc_report="$(echo $(ls -1tr ${ldir}/pipeline.*.report))"


function format_report1() {
    echo -e '### Report [1/3] - step details'
    echo '```'
    cat $proc_report
    echo '```'
}


pipeline=(
    # label                      # command line
    "report1                     format_report1"
)
