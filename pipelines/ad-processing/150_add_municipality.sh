. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/add_jobad_enrichments_api_results.out"
json_validate_output=1
retries=10

pipeline=(
    # label                      # command line
    "add_municipality_from_place $add_municipality_from_place"
)
