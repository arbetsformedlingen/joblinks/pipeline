. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/extract_brief_description.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "remove_description          $remove_description"
)
