. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/text2ssyk_bert.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "job_ad_hash                 $job_ad_hash"
)
