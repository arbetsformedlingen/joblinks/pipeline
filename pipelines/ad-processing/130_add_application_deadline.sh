. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/detect_language.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "add_application_deadline    $add_application_deadline"
)
