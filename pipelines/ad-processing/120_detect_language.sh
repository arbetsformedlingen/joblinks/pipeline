. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/filter_missing0.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "detect_language             $detect_language"
)
