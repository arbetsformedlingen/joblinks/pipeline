. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/job_ad_hash.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "remove_union_representatives_from_description $remove_union_representatives_from_description"
)
