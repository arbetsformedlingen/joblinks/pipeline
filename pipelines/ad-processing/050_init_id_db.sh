[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }


function set_default_db() {
    if [ ! -f "${ldir}/db" ]; then
        echo '"id","firstSeen"' >"${ldir}/db"
    fi
}


pipeline=(
    # label                      # command line
    "set_default_id_db           set_default_db"
)
