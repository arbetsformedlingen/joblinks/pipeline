. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/remove_union_representatives_from_description.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "extract_brief_description $extract_brief_description"
)
