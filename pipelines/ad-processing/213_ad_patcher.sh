. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/date_processor.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "ad_patcher                  $ad_patcher"
)
