. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/add_id.out"
json_validate_output=1

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "filter_missing0             $filter_missing_fields '.id,.originalJobPosting.description,.originalJobPosting.title' ${ldir}/filter_missing0.removed"
)
