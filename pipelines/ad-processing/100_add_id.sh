. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/prepare_ads.out"
json_validate_output=1

# instruct docker to share the database file
export DOCKERFLAGS="-v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "add_id                      $add_id '${ldir}/db'"
)
