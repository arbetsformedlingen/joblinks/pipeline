. "${proj_root}/conf/jobtechlinks.sh"

include_import_displayname="${include_import_displayname:-}"
flags=""
if [ -n "$include_import_displayname" ]; then
    flags="$flags --sources-to-import $include_import_displayname"
fi

pipeinput="${ldir}/ad_patcher.out"

pipeline=(
    # label                      # command line
    "filter_for_import           $filter_for_import $flags >/dev/null"
)
