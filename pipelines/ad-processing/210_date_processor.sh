. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/remove_description.out"
json_validate_output=1

pipeline=(
    # label                      # command line
    "date_processor              $date_processor"
)
