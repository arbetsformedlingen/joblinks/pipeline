. "${proj_root}/conf/jobtechlinks.sh"

[[ -n "$include_import" ]] || { echo "**** missing setting 'include_import'" >&2; exit 1; }

echo "remove_ads_for_statistics opt in/include: $include_import" >&2

pipeinput="${ldir}/filter_for_import.out"

json_validate_output=1
clitoolscmd="$($clitools --cmd)"

function quoted_jq() {
    include_import="${include_import:-}"
    if [ -n "$include_import" ]; then
        JQ="select("$(cat << EOF | perl
my @exl=split(/,/,"$include_import"); print join(" or ", map { ".originalJobPosting.scraper == \"\$_\"" } @exl);
EOF
)")"
        $clitoolscmd jq -c "$JQ"
    else
        cat -
    fi
}

pipeline=(
    # label                      # command line
    "remove_ads_for_statistics   quoted_jq"
)
