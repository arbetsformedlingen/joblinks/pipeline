# The purpose of this step is to: 1) prevent flooding the terminal with
# all ads when developing, and 2) get a reference to the output of the pipeline,
# that subsequent steps can reference, instead of using the last processing step
# as name, which would have to be updated in the subsequent steps.

[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

pipeinput="${ldir}/remove_ads_for_statistics.out"

json_validate_output=1

pipeline=(
    # label                      # command line
    "terminate                   cat -"
)
