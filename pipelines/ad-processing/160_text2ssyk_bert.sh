. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]                          || { echo "**** missing setting 'ldir'" >&2;      exit 1; }
[[ -n "$text2ssyk_bert_cache_file" ]]     || { echo "**** missing setting text2ssyk_bert_cache_file''" >&2;      exit 1; }
[[ -n "$text2ssyk_bert_class_key" ]]      || { echo "**** missing setting text2ssyk_bert_class_key''" >&2;      exit 1; }

pipeinput="${ldir}/add_municipality_from_place.out"
json_validate_output=1

# instruct docker to share the database file
export DOCKERFLAGS="-v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "text2ssyk_bert              $text2ssyk_bert --cache-file '${ldir}/${text2ssyk_bert_cache_file}' --class-key ${text2ssyk_bert_class_key}"
)
