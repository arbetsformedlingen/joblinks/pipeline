. "${proj_root}/conf/processors.sh"
[[ -n "$ldir" ]]      || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

. "${proj_root}/secrets/${test_or_prod}/enrich/user.sh"

jae_api_key=${jae_api_key:-}
[[ -n "$jae_api_key" ]] || { echo "**** missing settings 'jae_api_key'" >&2; exit 1; }

pipeinput="${ldir}/add_application_deadline.out"
json_validate_output=1
retries=10

pipeline=(
    # label                              # command line
    "add_jobad_enrichments_api_results   $add_jobad_enrichments_api_results --apikey ${jae_api_key}"
)
