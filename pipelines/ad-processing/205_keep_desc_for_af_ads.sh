. "${proj_root}/conf/jobtechlinks.sh"

pipeinput="${ldir}/extract_brief_description.out"
retries=10

# output may be 0 bytes
json_validate_output=0
size_validate_output=0

clitoolscmd="$($clitools --cmd)"


function quoted_jq_get_ads_with_desc() {
    include_full_desc_upload="${include_full_desc_upload:-}"

    if [ -n "$include_full_desc_upload" ]; then
        JQ="select("$(cat << EOF | perl
my @exl=split(/,/,"$include_full_desc_upload"); print join(" or ", map { ".originalJobPosting.scraper == \"\$_\"" } @exl);
EOF
)")"
        $clitoolscmd jq -c "$JQ"
    else
        cat - >/dev/null # don't risk spilling ads that are not set for inclusion
    fi
}
export -f quoted_jq_get_ads_with_desc


# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"


pipeline=(
    # label                      # command line
    "jobtechlinks_full_desc_ads  quoted_jq_get_ads_with_desc"
)
