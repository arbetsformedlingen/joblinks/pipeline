. "${proj_root}/conf/jobtechlinks.sh"
set -x
new_ads_coef="${new_ads_coef:-}"
importflags="--secrets /secrets/import-to-elastic/env.sh --namespace ${namespace}"
if [ -n "$new_ads_coef" ]; then
    importflags="$importflags --coef $new_ads_coef"
fi

pipeinput="${ldir}/terminate.out"
retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"

# Check for existence, as it will only be there for systems checked out with closed source.
if [ -f $import_to_elastic ]; then
pipeline=(
    # label                      # command line
    "import_to_elastic           $import_to_elastic $importflags >/dev/null"
)
else
pipeline=(
    # label                      # command line
    "import_to_elastic           echo no import_elastic checked out>&2"
)
fi
