. "${proj_root}/conf/jobtechlinks.sh"

new_ads_coef="${new_ads_coef:-}"
importflags="--secrets /secrets/joblinks-importer/env.sh --namespace ${namespace}"
if [ -n "$new_ads_coef" ]; then
    importflags="$importflags --coef $new_ads_coef"
fi

pipeinput="${ldir}/terminate.out"
retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"


function featureswitch_joblinks_importer() {
    local active_import="${active_joblinks_importer:-false}"

    if [ "$active_import" == "true" ]; then
	echo "**** feature switch active_joblinks_importer is on" >&2
        $joblinks_importer $importflags >/dev/null
    else
	echo "**** feature switch active_joblinks_importer is off" >&2
    fi
}
export -f featureswitch_joblinks_importer


pipeline=(
    # label                      # command line
    "joblinks_importer           featureswitch_joblinks_importer"
)
