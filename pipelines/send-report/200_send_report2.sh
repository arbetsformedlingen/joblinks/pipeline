. "${proj_root}/conf/jobtechlinks.sh"

pipeinput="${ldir}/report2.out"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"

pipeline=(
    # label                      # command line
    "chat_notify                 $chat_notify /app/msgsend.sh /secrets/mattermost/conf.sh /secrets/mattermost/mattersend.conf"
)
