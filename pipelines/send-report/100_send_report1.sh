. "${proj_root}/conf/jobtechlinks.sh"

pipeinput="${ldir}/report1.out"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"

pipeline=(
    # label                      # command line
    "chat_notify                 $chat_notify /app/filedropper.sh /secrets/mattermost/conf.sh 'Report_[1/3]_-_details'"
)
