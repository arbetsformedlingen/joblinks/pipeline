# Inputdata files in JSONL format, each line a Job Posting ad.
#
#
. "${proj_root}/conf/processors.sh"

[[ -n "$inputdata_localdir" ]] || { echo "**** missing setting 'inputdata_localdir'" >&2; exit 1; }

json_validate_output=1
clitoolscmd="$($clitools --cmd)"


function insert_filename() {
    for F in $(find "$inputdata_localdir" -type f -print); do
        local name="$(basename $F)"
        $clitoolscmd jq -c --arg name "$name" '.scraper = $name' < $F
    done
}


function quoted_jq() {
    $clitoolscmd jq -c '. | { originalJobPosting:.}'
}


pipeline=(
    # label                      # command line
    "aggregate                   insert_filename"
    "trunc                       trunc_jsonl"
    "prepare_ads                 quoted_jq"
)
