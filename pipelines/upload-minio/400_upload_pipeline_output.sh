. "${proj_root}/conf/jobtechlinks.sh"

upload="${ldir}/terminate.out"
retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "upload_priv_output          $rclone --secrets /secrets/rclone/rclone.conf --op copy --src ${upload} --trg '${privatebucket}:${privatebucket}/${output_file}'"
)
