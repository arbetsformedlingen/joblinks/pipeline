. "${proj_root}/conf/jobtechlinks.sh"
[[ -n "$text2ssyk_bert_cache_file" ]]     || { echo "**** missing setting text2ssyk_bert_cache_file''" >&2;      exit 1; }

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

if [ -f "${ldir}/${text2ssyk_bert_cache_file}" ]; then
    pipeline=(
        # label                      # command line
        "upload_text2ssyk_bert_db    $rclone --flock --secrets /secrets/rclone/rclone.conf --op copy --src '${ldir}/${text2ssyk_bert_cache_file}' --trg '${privatebucket}:${privatebucket}/${namespace}/${text2ssyk_bert_cache_file}'"
    )
else
    pipeline=(
        # label                      # command line
        "upload_text2ssyk_bert_db    echo No db-file found, skipping upload >&2"
    )
fi
