. "${proj_root}/conf/jobtechlinks.sh"

# it's called filter_for_import, but it is not the step that actually
# removes ads - it only marks them for removal
upload="${ldir}/filter_for_import.out"
retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "upload_priv_output          $rclone --secrets /secrets/rclone/rclone.conf --op copy --src ${upload} --trg '${privatebucket}:${privatebucket}/${full_output_file}'"
)
