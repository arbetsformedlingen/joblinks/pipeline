. "${proj_root}/conf/jobtechlinks.sh"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

if [ -f "${ldir}/db" ]; then
    pipeline=(
        # label                      # command line
        "upload_id_db                $rclone --flock --secrets /secrets/rclone/rclone.conf --op copy --src '${ldir}/db' --trg '${privatebucket}:${privatebucket}/${namespace}/db.csv'"
    )
else
    pipeline=(
        # label                      # command line
        "upload_id_db                echo No db-file found, skipping upload >&2"
    )
fi
