. "${proj_root}/conf/jobtechlinks.sh"

upload="${ldir}/prepare_ads.out"
retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "upload_priv_input           $rclone --secrets /secrets/rclone/rclone.conf --op copy --src ${upload} --trg '${privatebucket}:${privatebucket}/${input_file}'"
)
