. "${proj_root}/conf/jobtechlinks.sh"

function copy() {
    local src="$1"
    if [ "$(dirname $src)" != "${ldir}" ]; then
        cp "$src" "${ldir}"
    fi
}

pipeline=(
    # label                      # command line
    "import_mainlog              copy" # --arg import_mainlog /tmp/pipeline.log
)
