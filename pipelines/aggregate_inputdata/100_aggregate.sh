. "${proj_root}/conf/jobtechlinks.sh"

json_validate_output=1
retries=10

clitoolscmd="$($clitools --cmd)"

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"

function quoted_jq() {
    $clitoolscmd jq -c '.[] | { originalJobPosting:.}'
}

pipeline=(
    # label                      # command line
    "aggregate                   $data_aggregator --bucket ${publicbucket} --secretsdir /secrets"
    "trunc                       trunc"
    "prepare_ads                 quoted_jq"
)
