. "${proj_root}/conf/jobtechlinks.sh"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "upload_step_log             $rclone --secrets /secrets/rclone/rclone.conf --op sync --src ${ldir} --trg '${privatebucket}:${privatebucket}/${log_dir}'  -- --exclude \"/upload_step_log*\""
)
