. "${proj_root}/conf/jobtechlinks.sh"
[[ -n "$text2ssyk_bert_cache_file" ]]     || { echo "**** missing setting text2ssyk_bert_cache_file''" >&2;      exit 1; }
[[ -n "$ldir" ]]                          || { echo "**** missing setting 'ldir'" >&2;      exit 1; }

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "download_text2ssyk_bert_db  $rclone --flock --secrets /secrets/rclone/rclone.conf --op copy --src '${privatebucket}:${privatebucket}/${namespace}/${text2ssyk_bert_cache_file}' --trg '${ldir}/${text2ssyk_bert_cache_file}'"
)
