. "${proj_root}/conf/jobtechlinks.sh"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets -v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "download_id_db              $rclone --flock --secrets /secrets/rclone/rclone.conf --op copy --src '${privatebucket}:${privatebucket}/${namespace}/db.csv' --trg '${ldir}/db'"
)
