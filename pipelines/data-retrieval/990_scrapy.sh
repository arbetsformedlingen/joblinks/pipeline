. "${proj_root}/conf/jobtechlinks.sh"

retries=0

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}:/secrets"

# Check for existence, as it will only be there for systems checked out with closed source.
if [ -f $scraping ] ; then
pipeline=(
    # label                      # command line
    "scraping                    $scraping --secretsdir /secrets --bucket ${publicbucket} --discard-invalid"
)
else
pipeline=(
    # label                      # command line
    "scraping                    echo scraping not checked out >&2"
)
fi
