. "${proj_root}/conf/jobtechlinks.sh"

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${ldir}:${ldir}"

pipeline=(
    # label                      # command line
    "compress                    $clitools find ${ldir} -type f -size +0b -name \*.log -or -name \*.out | $clitools xargs gzip"
)
