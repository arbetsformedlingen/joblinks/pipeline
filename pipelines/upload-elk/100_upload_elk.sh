. "${proj_root}/conf/jobtechlinks.sh"

retries=10

# instruct docker to share the secrets directory with the container
export DOCKERFLAGS="-v ${proj_root}/secrets/${test_or_prod}/elk:/secrets -v ${ldir}:/logs"

pipeline=(
    # label                      # command line
    "upload_elk                  $upload_logs_elastic /logs /secrets '/filebeat_${test_or_prod}.yaml' >/dev/null" # last step, can discard output
)
