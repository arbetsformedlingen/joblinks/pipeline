#!/usr/bin/env bash

set -eEu -o pipefail

CONF=test-small-without-scrape.sh

#PIPEGOALS_SCRAPE="data-retrieval"
#PIPEGOALS_PROCESS="download-minio,aggregate_inputdata,ad-processing,integrations"
PIPEGOALS_PROCESS="aggregate_inputdata"
#PIPEGOALS_FINISH="upload-minio"

for G in "$PIPEGOALS_PROCESS" ; do
    bin/pipeline_group --conf conf/"$CONF" --groups $G -- --ldir /tmp/outputdir
done
