## Main repo
REPO="https://gitlab.com/arbetsformedlingen/joblinks/pipeline.git"

## Secrets repo (optional)
SECRETSREPO="https://gitlab.com/af-temp/joblinks-secrets.git"

## Place logs here
LOGDIR=/var/log/pipeline

## Default base directory for sessions (todo: read this setting from the pipeline jobtechlinks.sh)
PROJBASE=/tmp/pipeline

## The pipeline installation directory for deploys
# Warning: this directory will be autocleaned, so don't point it at something important
DEPLOYDIR=/tmp/pipeline_deploy


## List modules that are closed source
CLOSED_SOURCE_REPOS=""
