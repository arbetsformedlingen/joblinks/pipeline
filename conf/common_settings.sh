set -eEu

export include_import="arbetsformedlingen.se,careerbuilder.se,ingenjorsguiden.se,jobb.blocket.se,jobbdirekt.se,lararguiden.se,monster.se.xmlfeed,offentligajobb.se,onepartnergroup.se,studentjob.se,netjobs.com,traineeguiden.se,thehub.io,ingenjorsjobb.se.xmlfeed,intenso.se,ledigajobb.se"

export exclude_import="gronajobb.se,saljpoolen.se"

export include_import_displayname="arbetsformedlingen.se,careerbuilder.se,ingenjorsguiden.se,jobb.blocket.se,jobbdirekt.se,lararguiden.se,monster.se,offentligajobb.se,onepartnergroup.se,studentjob.se,netjobs.com,traineeguiden.se,thehub.io,ingenjorsjobb.se.xmlfeed,intenso.se,ledigajobb.se"

export exclude_import_displayname="gronajobb.se,saljpoolen.se"



export include_full_desc_upload="arbetsformedlingen.se"


# define which sites that need Crawlera proxy
SITES_CRAWLERA="offentligajobb.se,careerbuilder.se,thehub.io"


text2ssyk_bert_cache_file=text2ssyk_bert.sqlite
text2ssyk_bert_class_key=ssyk_lvl4


# Local output directory, where result files are produced. Leave empty
# to use default /tmp/pipeline/$namespace
ldir=


##### PROCESSOR SPECIFIC SETTINGS

### for: aggregate_inputdata/100_aggregate.sh  data-retrieval/990_scrapy.sh


### for: aggregate_inputdata_localdir/100_aggregate.sh
inputdata_localdir=/var/tmp/indata


### for: ad-processing/140_enrich.sh
# Enter jobad enrichment key (get key here: https://apirequest.jobtechdev.se/).
# Concerns this program https://gitlab.com/arbetsformedlingen/joblinks/add-jobad-enrichments-api-results/
#
#jae_api_key=
#
# It can also be loaded from a private file:
if [ -f secrets/test/enrich/user.sh ]; then
    . secrets/test/enrich/user.sh
fi

### for: pipelines/integrations/100_import_elastic.sh
# include_import - see above

### for: pipelines/integrations/100_import_elastic.sh
#new_ads_coef=0.1
