# if you rely on already set variables below, start by verifying they are set
[[ -n "$USER" ]]      || { echo "**** missing setting 'USER'" >&2;      exit 1; }
[[ -n "$namespace" ]] || { echo "**** missing setting 'namespace'" >&2; exit 1; }
[[ -n "$publicbucket" ]] || { echo "**** missing setting 'publicbucket'" >&2; exit 1; }
[[ -n "$privatebucket" ]] || { echo "**** missing setting 'privatebucket'" >&2; exit 1; }

. "${proj_root}"/conf/processors.sh

# required setting ldir, local output directory
ldir="${ldir:-/tmp/pipeline/$namespace}"

# only generate the timestamp once, for consistency in the output file names
today=$(date +%Y-%m-%d)

# Minio base folder for uploads
minio_base="$namespace/$today"

# Upload the scraped file
public_input_file="aggregator/$namespace/$today.json"

# Minio targets for the pipeline input and output
full_output_file="${minio_base}/output-statistics.json" # the unfiltered output including ads for statistics
output_file="${minio_base}/output.json" # the output from the final pipeline processor
input_file="${minio_base}/input.json"   # pipeline input is the scraped, aggregated file
log_dir="${minio_base}/logs"

# Used to select sub folder in secrets-repo. Override with 'prod' from invocation script.
test_or_prod=${test_or_prod:-test}
