# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=pipeline-test
PUBBUCKET=scrapinghub-test
PRIVBUCKET=pipeline-test

# some optional flags:
PIPEFLAGS=""

CRAWLERA="--arg scraping --sites-use-crawlera --arg scraping $SITES_CRAWLERA"
#SITES_CRAWLERA+=""
#exclude_import+=""
#include_import+=""

. "${conf_dir}/common_settings_interpolate.sh"
