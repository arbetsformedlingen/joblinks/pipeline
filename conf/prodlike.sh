# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=pipeline
PUBBUCKET=scrapinghub-test
PRIVBUCKET=pipeline-test

# Feature switches
active_joblinks_importer=true

# some optional flags:
PIPEFLAGS="--arg import_to_elastic --env --arg import_to_elastic ES_TAX_INDEX_ALIAS=taxonomyv21"

CRAWLERA=""

. "${conf_dir}/common_settings_interpolate.sh"
