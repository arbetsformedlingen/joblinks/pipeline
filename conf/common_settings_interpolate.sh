set -eEu

# activate these scrapers
SITES="$include_import,$exclude_import"

PIPEFLAGS="--namespace $NAMESPACE \
           --publicbucket $PUBBUCKET \
           --privatebucket $PRIVBUCKET \
           --arg scraping  --sites --arg scraping $SITES \
           --arg aggregate --sites --arg aggregate $SITES \
           --arg import_mainlog ~/out.log $CRAWLERA \
           $PIPEFLAGS"
