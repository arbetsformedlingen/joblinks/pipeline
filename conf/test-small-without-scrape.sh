# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=test-small-without-scrape
PUBBUCKET=scrapinghub-test
PRIVBUCKET=pipeline-test

# Feature switches
active_joblinks_importer=false

# some optional flags:
PIPEFLAGS="-l 65 --arg import_to_elastic --env --arg import_to_elastic ES_TAX_INDEX_ALIAS=taxonomyv21"

#SITES_CRAWLERA+=""
#exclude_import+=""
#include_import+=""
CRAWLERA=""

. "${conf_dir}/common_settings_interpolate.sh"
