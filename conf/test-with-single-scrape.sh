# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=test-with-single-scrape
PUBBUCKET=pipeline-test
PRIVBUCKET=pipeline-test

# Feature switches
active_joblinks_importer=false

# some optional flags:
PIPEFLAGS="--arg import_to_elastic --env --arg import_to_elastic ES_TAX_INDEX_ALIAS=taxonomyv21 --skip import_mainlog --arg scraping --scrape"

export include_import="onepartnergroup.se"
export exclude_import="gronajobb.se"
export include_import_displayname="onepartnergroup.se"
export exclude_import_displayname="gronajobb.se"
CRAWLERA=""

. "${conf_dir}/common_settings_interpolate.sh"
