# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=pw-dev3
PUBBUCKET=pipeline-test
PRIVBUCKET=pipeline-test


# some optional flags:
#--skip import_to_elastic
PIPEFLAGS="--arg scraping --limit-scrape -l 55 --skip import_mainlog"

# enable scraping:
PIPEFLAGS="$PIPEFLAGS --arg scraping --scrape"


#SITES_CRAWLERA+=""
#exclude_import+=""
include_import+=",2020_10.jsonl,real10.jsonl"

. "${conf_dir}/common_settings_interpolate.sh"
