# Detect the directory name of this script
conf_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Start with the prod conf
. "${conf_dir}/common_settings.sh"

NAMESPACE=test-small-with-scrape
PUBBUCKET=pipeline-test
PRIVBUCKET=pipeline-test


# some optional flags:
PIPEFLAGS="-l 55"

# enable scraping:
PIPEFLAGS="$PIPEFLAGS --arg scraping --scrape"


export include_import="monster.se.xmlfeed"
export exclude_import="gronajobb.se"
export include_import_displayname="monster.se"
export exclude_import_displayname="gronajobb.se"

CRAWLERA="--arg scraping --sites-use-crawlera --arg scraping $SITES_CRAWLERA"

. "${conf_dir}/common_settings_interpolate.sh"
