#!/usr/bin/env bash
set -eEu -o pipefail

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

CONF_OPS="$self_dir"/../conf/ops.sh


if [ ! -f "$CONF_OPS" ]; then
    echo "**** cannot find $CONF_OPS" >&2; exit 1
fi


##################################################


function checkout_no_closedsource() {
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')

    "$self_dir"/../bin/devops --commit HEAD --conf "$CONF_OPS" checkout \
        || { echo "*** checkout failed" >&2; exit 1; }

    if [ ! -f "$DEPLOYD"/bin/deploy.sh ]; then
        echo "*** checkout failed, cannot find the files in $DEPLOYD" >&2; exit 1;
    fi
}


function checkout_closedsource() {
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')

    . secrets/test/gitlab-deploy/secrets.sh || exit 1

    "$self_dir"/../bin/devops --commit HEAD --conf "$CONF_OPS" --closedsource checkout \
        || { echo "*** checkout failed" >&2; exit 1; }

    if [ ! -f "$DEPLOYD"/bin/deploy.sh ]; then
        echo "*** checkout failed, cannot find the files in $DEPLOYD" >&2; exit 1;
    fi

    if [ ! -f "$DEPLOYD"/processors/scraping/run.sh ]; then
        echo "*** checkout failed, cannot find the closed source file" >&2; exit 1;
    fi
}



function build() {
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')

    cd "$DEPLOYD" || { echo "**** no directory $DEPLOYD" >&2; exit 1; }

    "$DEPLOYD"/bin/devops --conf "$CONF_OPS" build \
        || { echo "*** build failed" >&2; exit 1; }
}


function clean() {
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')

    if [ -d "$DEPLOYD" ]; then
        "$self_dir"/../bin/devops --conf "$CONF_OPS" clean \
            || { echo "*** clean failed" >&2; exit 1; }
    fi

    if [ -d "$DEPLOYD" ]; then
        echo "**** clean: $DEPLOYD still exists" >&2; exit 1
    fi
}


function clean_workdir() {
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')
    local PROJBASE=$(bash -c '. '"$CONF_OPS"' && echo $PROJBASE')
    local CONF_USER="$self_dir/conf/run0.sh"
    local NAMESPACE=$(bash -c '. '"$CONF_USER"' && echo $NAMESPACE')

    if [ -d "$PROJBASE/$NAMESPACE" ]; then
        "$self_dir"/../bin/devops --conf "$CONF_OPS" --namespace "$NAMESPACE" clean_workdir \
            || { echo "*** clean failed" >&2; exit 1; }
    fi

    if [ -d "$PROJBASE/$NAMESPACE" ]; then
        echo "**** $PROJBASE/$NAMESPACE still exists" >&2; exit 1;
    fi
}



function setup_datadir() {
    local DATADIR="$1"

    # Copy testdata to the data directory specified in the userconf
    if [ -z "$DATADIR" ]; then
        echo "**** $DATADIR not set" >&2; exit 1
    fi

    if [ -d "$DATADIR" ]; then
        echo "**** $DATADIR already exists" >&2; exit 1
    fi

    mkdir "$DATADIR" || exit 1
    #trap "rm -f $DATADIR/* ; rmdir $DATADIR" EXIT
    cp "$DEPLOYD/test/data/in_no_originaljobposting.jsonl" "$DATADIR"/ || exit 1
}




function run() {
    local conf="$1"
    local groups="$2"
    local DEPLOYD=$(bash -c '. '"$CONF_OPS"' && echo $DEPLOYDIR')
    local CONF_USER="$DEPLOYD"/test/conf/"$conf"
    local DATADIR=$(bash -c '. '"$CONF_USER"' && echo $inputdata_localdir')

    setup_datadir "$DATADIR"

    "$DEPLOYD"/bin/pipeline_group --conf "$CONF_USER" \
               --input "$DEPLOYD"/data/in_no_originaljobposting.jsonl \
               --groups "$groups" \
        || { echo "*** run failed" >&2; exit 1; }


    #rm -f "$DATADIR"/*
    #rmdir "$DATADIR"
    #trap - EXIT
}



##################################################

#clean_workdir
#clean

#checkout_no_closedsource
#checkout_closedsource
#build
#run aggregate_inputdata_localdir
run run0.sh hello-world
#run run0.sh aggregate_inputdata_localdir,ad-processing
#run run1.sh aggregate_inputdata_localdir,ad-processing
exit
clean_workdir
clean
