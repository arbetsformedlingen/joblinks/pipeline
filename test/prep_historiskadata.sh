#!/bin/bash

NR_OF_ADS_TO_EXTRACT=10


# download and uncompress archive
wget https://minio.arbetsformedlingen.se/historiska-annonser/2020.rar
unrar x 2020.rar

# copy attribute headline to title, make description a text node, and convert to jsonl
jq -c '.[] | select(.description.text) | .title=.headline | .org_description=.description | .description=.org_description.text' < 2020.json > 2020.jsonl

# keep a number of ads
head -n $NR_OF_ADS_TO_EXTRACT < 2020.jsonl > 2020_"$NR_OF_ADS_TO_EXTRACT".jsonl
#split -d -a 3 -l 80000 2020.jsonl 2020_jsonl_
