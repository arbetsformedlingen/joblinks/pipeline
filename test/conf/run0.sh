NAMESPACE=pw-dev0
PUBBUCKET=pipeline-test
PRIVBUCKET=pipeline-test

PIPEFLAGSTEST="-l 55 --skip import_to_elastic --arg scraping --limit-scrape"
PIPEFLAGS="--namespace $NAMESPACE \
           --publicbucket $PUBBUCKET \
           --privatebucket $PRIVBUCKET \
           $PIPEFLAGSTEST"


# Local output directory, where result files are produced. Leave empty
# to use default /tmp/pipeline/$namespace
ldir=/tmp/pipeline_test_"$USER"


##### PROCESSOR SPECIFIC SETTINGS

### for: aggregate_inputdata_localdir/100_aggregate.sh
inputdata_localdir=/tmp/pipeline_test_"$USER"
